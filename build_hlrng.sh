#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

# LOAD REQUIRED MODULES
module load intel-oneapi-compilers/2023.2.1
module load intel-oneapi-mpi/2021.10.0
module load netcdf-fortran/4.6.1-mpi

# search for netcdf-fortran paths in environment variables
export IOW_ESM_NETCDF_INCLUDE=`env | grep ^INCLUDE= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`
export IOW_ESM_NETCDF_LIBRARY=`env | grep ^LIBRARY_PATH= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`

export IOW_ESM_FC="mpiifort"
export IOW_ESM_CC="mpiicc"
export IOW_ESM_LD="mpiifort"

export IOW_ESM_MAKE="/usr/bin/make"

# GET IOW ESM ROOT PATH
export IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# compile mode: "PRODUCTION" or "DEBUG"
if [ $debug == "debug" ]; then
	export IOW_ESM_COMPILE_MODE="DEBUG"
elif [ $debug == "release" ]; then
	export IOW_ESM_COMPILE_MODE="PRODUCTION"
else
	echo "Compile mode is not specified correctly. Use debug or release"
	exit;
fi

if [ $debug == "debug" ]; then
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM -DIOW_ESM_DEBUG"

	export IOW_ESM_FFLAGS="-O0 -r8 -g -traceback -check all -fp-model precise -xHost -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O0 -g -traceback -fp-model precise -xHost -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-g -traceback -L${IOW_ESM_NETCDF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDF_LIBRARY}"
else
	# compiler flags
	export IOW_ESM_CPPDEFS="-DOASIS_IOW_ESM"
	
	export IOW_ESM_FFLAGS="-O3 -r8 -align -pad -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_CFLAGS="-O3 -align -static-intel -fp-model precise -qopt-zmm-usage=high -xCORE-AVX512 -I${IOW_ESM_NETCDF_INCLUDE}/"
	export IOW_ESM_LDFLAGS="-L${IOW_ESM_NETCDF_LIBRARY} -lnetcdf -lnetcdff -Wl,-rpath,${IOW_ESM_NETCDF_LIBRARY}"
fi

# MAKE CLEAN
if [ $rebuild == "rebuild" ]; then
	rm -rf ${IOW_ESM_ROOT}/components/MOM5/exec/IOW_ESM_${IOW_ESM_COMPILE_MODE}
fi

# RUN BUILD COMMAND
cd ${IOW_ESM_ROOT}/components/MOM5/exp
./MOM_compile.csh
cd ${IOW_ESM_ROOT}/components/MOM5

